# DrewOS dev #
I'm going to write an OS because why not?

## Testing it Out ##
Clone the git repository and run:

    make                # to build the kernel
    make run            # to run qemu with the proper arguments
    # optionally:
    sudo make install   # to install the kernel and ramdisk to your /boot directory

This will build the kernel and the ramdisk and, optionally, copy them to your boot directory so you can add them to your grub config. As of 22 May 2016, a bootdisk is no longer created and the git repository no longer carries a GRUB disk. Building a bootable floppy has been deprecated in favor of running with QEMU's built-in multiboot support.

## Goals/Roadmap ##
The basic goal of this project is to create a mostly POSIX-compatible kernel 
from scratch. Most of the focus will be on generic hardware compatibility and 
standard specs such as VESA. Eventually some complex hardware should be 
supported.

### Basic Goals ###
* Create a functional modular kernel capable of executing ELF binaries
* Create some sort of C standard library
* POSIX thread support
* File system support (e.g. EXT2/3/4)
* VESA mode support for optimal graphics resolution
* Basic networking

### Wishlist ###
* Dynamic library loader
* Working window server like X11, but not X11
* Port QT and/or GTK and some apps
* Audio support
* Btrfs support
* Support for my own hardware (possible to support more eventually)
  * Some form of Nouveau-like graphics driver for NVIDIA GeForce GTX 760 (I 
have this hardware)
  * Realtek 8169 (I have this hardware)

### Roadmap ###
This kernel is currently capable of reading its multiboot params, which is not 
super useful. The current roadmap is as follows:

* Finish basic functionality:
    * Paging *done*
    * Heap *done, implemented with klmalloc*
    * VFS *done*
    * Initial RAM disk *working, not perfect*
    * Multitasking (we are not DOS!)
    * User mode
    * Loading and executing ELF binaries in user mode
    * Complete system calls
    * users and groups?
* Write a file system driver for a real file system
    * Target is EXT2/3/4, might use FAT *can read small EXT2 partitions from RAM*
    * Implement OS image as a QEMU compatible virtual drive
* VESA mode handler
    * Needs virtual 8086 monitor
    * Able to use gfx modes and have output, so needs a fb terminal driver
* Some basic GNU tools
    * bash shell (because I like bash)
    * ls, mv, rm, etc.
    * perl?
* Networking
    * IPv4 (and maybe v6)
    * Driver for both QEMU and Realtek 8169

## License ##

DrewOS is under the NCSA license, which is a derivative (and fully compatible with) the BSD license. It is also forward compatible with the GPL, so you can use DrewOS bits and pieces in GPL. The terms of the license are listed here for your convience:

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal with the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
      1. Redistributions of source code must retain the above copyright notice,
         this list of conditions and the following disclaimers.
      2. Redistributions in binary form must reproduce the above copyright
         notice, this list of conditions and the following disclaimers in the
         documentation and/or other materials provided with the distribution.
      3. Neither the names of the ToAruOS Kernel Development Team, Kevin Lange, Drew Walton,
         nor the names of its contributors may be used to endorse
         or promote products derived from this Software without specific prior
         written permission.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    WITH THE SOFTWARE.
