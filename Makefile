#CC = gcc
CC = clang
# Sometimes we just have to use GCC
GCC = gcc
# CFLAGS for core components
CFLAGS = -Wall -Wextra -pedantic -m32 -O0 -std=c99 -fno-stack-protector -nostdinc -ffreestanding -Wno-unused-function -Wno-unused-parameter -g -Wno-newline-eof
# CFLAGS for native utils
NATIVEFLAGS = -std=c99 -g -pedantic -Wall -Wextra -Wno-unused-parameter -Wno-newline-eof
# Linker for core
LD = ld -m elf_i386
YASM = yasm
ECHO = `which echo` -e
# Feel free to be specific, but I'd rather you not be.
MODULES = $(patsubst %.c,%.o,$(wildcard kernel/core/*.c))
FILESYSTEMS = $(patsubst %.c,%.o,$(wildcard kernel/core/fs/*.c))
EMU = qemu-system-i386
GENEXT = genext2fs
DD = dd conv=notrunc

.PHONY: all system clean install run

all: system bootdisk.img
system: drewos-initrd drewos-kernel

install: drewos-initrd drewos-kernel
	@${ECHO} -n "\033[34m   --   Installing to /boot...\033[0m"
	@cp drewos-kernel /boot/drewos-kernel
	@cp drewos-initrd /boot/drewos-initrd
	@${ECHO} "\r\033[34;1m   --   Kernel and ramdisk installed.\033[0m"

run: drewos-kernel drewos-initrd
	${EMU} -kernel drewos-kernel -initrd drewos-initrd -serial stdio

################
#    Kernel    #
################
drewos-kernel: kernel/start.o kernel/link.ld kernel/main.o ${MODULES} ${FILESYSTEMS}
	@${ECHO} -n "\033[32m   LD   $<\033[0m"
	@${LD} -T kernel/link.ld -o drewos-kernel kernel/*.o kernel/core/*.o kernel/core/fs/*.o
	@${ECHO} "\r\033[32;1m   LD   $<\033[0m"
	@${ECHO} "\033[34;1m   --   Kernel is ready!\033[0m"

kernel/start.o: kernel/start.s
	@${ECHO} -n "\033[32m  yasm  $<\033[0m"
	@${YASM} -f elf -o kernel/start.o kernel/start.s
	@${ECHO} "\r\033[32;1m  yasm  $<\033[0m"

%.o: %.c
	@${ECHO} -n "\033[32m   CC   $<\033[0m"
	@${CC} ${CFLAGS} -I./kernel/include -c -o $@ $<
	@${ECHO} "\r\033[32;1m   CC   $<\033[0m"

################
#   Ram disk   #
################
drewos-initrd: initrd/boot/kernel initrd/boot/stage2
	@${ECHO} -n "\033[32m initrd Generating initial RAM disk\033[0m"
	@# Get rid of the old one
	@-rm -f drewos-initrd
	@${GENEXT} -d initrd -q -b 249 drewos-initrd
	@${ECHO} "\r\033[32;1m initrd Generated initial RAM disk image\033[0m"
	@${ECHO} "\033[34;1m   --   HDD image is ready!\033[0m"

### Ram Disk installers...

# Second stage bootloader
initrd/boot/stage2: bootloader/stage2.bin
	@mkdir -p initrd/boot
	@cp bootloader/stage2.bin initrd/boot/stage2

# Kernel
initrd/boot/kernel: drewos-kernel
	@mkdir -p initrd/boot
	@cp drewos-kernel initrd/boot/kernel

################
#   Utilities  #
################
util/bin/mrboots-installer: util/mrboots-installer.c
	@${ECHO} -n "\033[32m   CC   $<\033[0m"
	@${CC} ${NATIVEFLAGS} -o $@ $<
	@${ECHO} "\r\033[32;1m   CC   $<\033[0m"

################
#  Bootloader  #
################

# Stage 1
bootloader/stage1/main.o: bootloader/stage1/main.c
	@${ECHO} -n "\033[32m   CC   $<\033[0m"
	@${GCC} ${CFLAGS} -c -o $@ $<
	@${ECHO} "\r\033[32;1m   CC   $<\033[0m"

bootloader/stage1/start.o: bootloader/stage1/start.s
	@${ECHO} -n "\033[32m  yasm  $<\033[0m"
	@${YASM} -f elf32 -p gas -o $@ $<
	@${ECHO} "\r\033[32;1m  yasm  $<\033[0m"

bootloader/stage1.bin: bootloader/stage1/main.o bootloader/stage1/start.o bootloader/stage1/link.ld
	@${ECHO} -n "\033[32m   ld   $<\033[0m"
	@${LD} -o bootloader/stage1.bin -T bootloader/stage1/link.ld bootloader/stage1/start.o bootloader/stage1/main.o
	@${ECHO} "\r\033[32;1m   ld   $<\033[0m"

# Stage 2
bootloader/stage2/main.o: bootloader/stage2/main.c
	@${ECHO} -n "\033[32m   CC   $<\033[0m"
	@${GCC} ${CFLAGS} -I./kernel/include -c -o $@ $<
	@${ECHO} "\r\033[32;1m   CC   $<\033[0m"

bootloader/stage2/start.o: bootloader/stage2/start.s
	@${ECHO} -n "\033[32m  yasm  $<\033[0m"
	@${YASM} -f elf32 -p gas -o $@ $<
	@${ECHO} "\r\033[32;1m  yasm  $<\033[0m"

bootloader/stage2.bin: bootloader/stage2/main.o bootloader/stage2/start.o bootloader/stage2/link.ld
	@${ECHO} -n "\033[32m   ld   $<\033[0m"
	@${LD} -o bootloader/stage2.bin -T bootloader/stage2/link.ld bootloader/stage2/start.o bootloader/stage2/main.o
	@${ECHO} "\r\033[32;1m   ld   $<\033[0m"

##############
#  bootdisk  #
##############
bootdisk.img: bootloader/stage1.bin bootloader/stage2.bin util/bin/mrboots-installer
	@${ECHO} -n "\033[34m   --   Building bootdisk.img...\033[0m"
	@cat bootloader/stage1.bin bootloader/stage2.bin > bootdisk.img
	@${ECHO} "\r\033[34;1m   --   Bootdisk is ready!      \033[0m"

###############
#    clean    #
###############
clean:
	@${ECHO} -n "\033[31m   RM   Cleaning... \033[0m"
	@-rm -f drewos-kernel
	@-rm -f drewos-initrd
	@-rm -f kernel/*.o
	@-rm -f kernel/core/*.o
	@-rm -f kernel/core/fs/*.o
	@-rm -f bootloader/stage1.bin
	@-rm -f bootloader/stage1/*.o
	@-rm -f bootloader/stage2.bin
	@-rm -f bootloader/stage2/*.o
	@-rm -rf initrd/boot
	@-rm -f bootdisk.img
	@-rm -f util/bin/*
	@${ECHO} "\r\033[31;1m   RM   Finished cleaning.\033[0m\033[K"
